
-- SUMMARY --

This module is an add-on for the Closed Question module. It adds a form element
to the Closed Question's edit form "Do not load answer". When checked, Closed
Question will not load answers previously saved by the user.

-- REQUIREMENTS --

The Closed Question module: http://drupal.org/project/closedquestion, currently
only the 7.x-2.x-dev branch is supported.

-- INSTALLATION --

* Install as usual,
  see https://www.drupal.org/documentation/install/modules-themes/modules-7.

-- CONFIGURATION --

* None needed
